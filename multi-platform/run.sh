#!/bin/sh

YAML=""

case "$1" in
    "reset")
	YAML="reset.yml"
	shift
	;;
    "-h"|"help")
	echo "Syntax: run.sh [command] [options]"
	echo "  Commands:"
	echo "    reset: removes any state files that cause playbooks to be skipped"
	echo "    help:  display this message"
	exit 1
	;;
    *)
	YAML="site.yml"
	;;
esac

# For debugging!
#ansible-playbook -v -i hosts --syntax-check ${YAML} -e 'ansible_python_interpreter=/usr/local/bin/python' --ask-become-pass
#ansible-playbook -vvv -i hosts ${YAML} -e 'ansible_python_interpreter=/usr/local/bin/python' --ask-become-pass

# Runs all hosts, with [options] inline
ansible-playbook -vv -i hosts "$@" ${YAML} --ask-become-pass
