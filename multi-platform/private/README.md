This directory is for private data, such as 'deploy' SSH private keys.

Ideally this does not include any credentials that could be used for
interactive or privileged operations.  A version control repo is not
a great place to store secure data!
